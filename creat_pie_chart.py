"""
This software is the property of lululemon athletica and requires
expressed permission for use or distribution. All code is considered
proprietary and confidential to the extent allowed under applicable
software licenses.
"""

__author__ = "Seth Gilchrist"
__email__ = "sgilchrist@lululemon.com"
__copyright__ = "Copyright 2016, lululemon athletica, Vancouver, BC, Canada"
__license__ = "restricted"

import xlrd
from tkinter import filedialog
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle


input_file = r"/Users/sgilchrist/Documents/OneDrive - lululemon athletica/WhiteSpace_Pulse_Check_July 21.xlsx"
workbook = xlrd.open_workbook(input_file)
sheet = workbook.sheet_by_index(0)

safety = []
dependability = []
structure = []
meaning = []
impact = []

for row in range(3, sheet.nrows):
    safety.append(sheet.cell(row, 9).value)
    dependability.append(sheet.cell(row, 10).value)
    structure.append(sheet.cell(row, 11).value)
    meaning.append(sheet.cell(row, 12).value)
    impact.append(sheet.cell(row, 13).value)

options = ['1 - Completely Disagree',
           '2 - Disagree',
           '3 - Neither Agree nor Disagree',
           '4 - Agree',
           '5 - Completely Agree']
colours = ['r', 'y', 'b', 'c', 'g']

# get the number of each result in the selections for the pie charts
safety_results = [safety.count(i) for i in options]
dependability_results = [dependability.count(i) for i in options]
structure_results = [structure.count(i) for i in options]
meaning_results = [meaning.count(i) for i in options]
impact_results = [impact.count(i) for i in options]


## plot the pie charts
figH = plt.figure(0, figsize=(12, 8))
axH = [figH.add_subplot(2, 3, 1, aspect=1),
       figH.add_subplot(2, 3, 2, aspect=1),
       figH.add_subplot(2, 3, 3, aspect=1),
       figH.add_subplot(2, 3, 4, aspect=1),
       figH.add_subplot(2, 3, 5, aspect=1)]

plt.sca(axH[0])
plt.pie(safety_results, colors=colours)
plt.gca().set_title('Safety', fontsize=22)

plt.sca(axH[1])
plt.pie(dependability_results, colors=colours)
plt.gca().set_title('Dependability', fontsize=22)

plt.sca(axH[2])
plt.pie(structure_results, colors=colours)
plt.gca().set_title('Structure', fontsize=22)

plt.sca(axH[3])
plt.pie(meaning_results, colors=colours)
plt.gca().set_title('Meaning', fontsize=22)

plt.sca(axH[4])
pie_0 = plt.pie(impact_results, colors=colours)
plt.gca().set_title('Impact', fontsize=22)

## create the legend
# create dummy patch to add data the total number of responses to the legend
extra = Rectangle((0, 0), 1, 1, fc='w', fill=False, edgecolor='none', linewidth=0)
legend_text = ["Total Responses = {0}".format(len(safety))]
for option in options:
    legend_text.append(option.split(' - ')[1])
legend_patches = [extra]
for patch in pie_0[0]:
    legend_patches.append(patch)
plt.legend(legend_patches, legend_text, bbox_to_anchor=(1.05, .5), loc=6)

plt.show()
